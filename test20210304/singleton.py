#coding:utf-8
import time
from memory_profiler import profile
class singletonAndGCtest(object):
    singleobj=0
    _instance=None
    #类对象，所有的这个类的对象共享
    def __new__(cls, *args, **kwargs):
        #创建对象时调用
        if cls.singleobj>=1:
            pass
            # print("相关类对象已经创建，请勿重复创建")
        else:
            cls.singleobj+=1
            #利用object创建对象,不然实际没有对象被创建，对象的方法也不会被引用
            #只有调用了这个super创建完函数，才会去调用init
            cls._instance= super(singletonAndGCtest, cls).__new__(cls)
        # print("already create obj num is :%s" %cls.singleobj)
        # #一个下滑表示允许继承的子孙引用
        #如果之前没有创建，就创建返回，否则就返回已经创建的
        return cls._instance

    def __init__(self,b):
        self.b=b
    # def printnum(self):

    #     print("print self.b  value is %s" %self.b)
@profile
def createobj():
    for i in range(10000):
        obj=singletonAndGCtest(i)
        # print(id(obj))
        # if i%100==0:
        #     time.sleep(1)
        # print(dir(obj))1221
        # print(str(obj.printnum())  )
     
if __name__ == '__main__':
        createobj()

