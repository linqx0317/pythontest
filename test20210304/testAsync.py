#coding:utf-8
"""测试协程函数与协程对象"""
import asyncio
import datetime

async def func(val):
    """定义一个协程函数"""
    begintime=datetime.datetime.now().strftime("%c")
    print(begintime+":"+func.__name__+":"+str(val)+"begin")
    res=await asyncio.sleep(2)#等待协程睡眠结束
    print(func.__name__+":"+str(val)+" end,res is :"+str(res))
    endtime = datetime.datetime.now().strftime("%c")
    print(endtime + ":" + func.__name__ + ":" + str(val) + "begin")

res=func(1)
if __name__ == '__main__':
    asyncio.run(res)



