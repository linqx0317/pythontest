#coding:utf-8
#多个协程同时工作，创建task与直接放入事件触发器两种方式的实现
import asyncio
import datetime
async def func(i):
    print("current func is %s ,func begin,now time is :%s" %(i,datetime.datetime.now().strftime("%c")))
    await asyncio.sleep(i)
    print("current func is %s ,func end,now time is :%s" %(i,datetime.datetime.now().strftime("%c")))
    return i
async def  main():
    task = []
    for i in range(3):
        #创建的 同时他会把task扔进事件处理器
        taele=asyncio.create_task(func(i))
        task.append(taele)
    done, pending = await asyncio.wait(task)
    for ele in done:
        print("result is :%s" %ele.result)
        res=ele._result
    print(str(done))
if __name__ == '__main__':
    #写法1
    task = []
    for i in range(3):
        task.append(func(i))
    done, pending = asyncio.run(asyncio.wait(task))
    print(done)
    #写法2
    main()
