#coding:utf-8


class xrange(object):
    def __init__(self,num):
        self.num=num
    def __iter__(self):
        i=0
        while i<self.num:
            yield i#如果没有写i就没有返回给调用端的内容
            # b=yield i
            #如果写成单独写成一个方法，用这种写法可以接收到协程调用send发送过来的值
            # print(str(b))
            i+=1

if __name__ == '__main__':
    xran=xrange(10)
    for t in xran:#迭代器是调用迭代器名称.next()
        #如果调用迭代器.send(发送的内容)，可以往迭代器里面传送数据
        #也就相当调用了next且发送了数据，如果是单独的一个生成器方法，而不是放在iter里面
        print(str(t))
        # xran.send(t)

