#coding:utf-8
#合并两个列表并去除重复元素？
list1 = ['b','c','c','a','f','r','y','e','e']
list2 = ['t','y','x','y','z','e','f']
list1.extend(list2)
list1=set(list1)
print(list1)

#如何查询和替换一个文本中的字符串?
tempstr = "hello python,you,me,world"
print(tempstr.replace("hello","python"))
#还可以使用正则,有个sub()
tempstr = "hello python,you,me,world"
import re
rex = r'(hello|Use)'
print(re.sub(rex,"HELLO",tempstr))
