from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_boston
#from sklearn.externals import joblib #
import joblib
import numpy
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression,SGDRegressor,Ridge#正规方程，梯度下降
boston=load_boston()
#拆分测试集与训练集
data_train,data_test,aim_train,aim_test=train_test_split(boston.data,boston.target,test_size=0.2)
#测试集标准化
trainstd=StandardScaler()
data_trainstd=trainstd.fit_transform(data_train)
trainstd.fit(data_test)
data_teststd=trainstd.transform(data_test)
#print(data_teststd)
#训练集标准化，因为线性回归结果是w*x，所以测试的时候特征和目标都需要标准化
aimstd=StandardScaler()
aim_trainnum=numpy.array(aim_train)
aim_trainstd=aimstd.fit_transform(aim_trainnum.reshape(-1,1))
aim_testnum=numpy.array(aim_test)
#这里数据要指定形状，放进numpy里面进行转换
aimstd.fit(aim_testnum.reshape(-1,1))
aim_teststd=aimstd.transform(aim_testnum.reshape(-1,1))
#测试正规方程
lr=LinearRegression()
lr.fit(data_trainstd,aim_trainstd)
lrres=lr.predict(data_teststd)
#梯度下降
sg=SGDRegressor()
sg.fit(data_trainstd,aim_trainstd)
sgres=sg.predict(data_teststd)
lrres=aimstd.inverse_transform(lrres)#正规方程结果,
#正规方程的结果每一个结果都是一个数组
#梯度下降的结果是所有的结果都在一个数组里
lrresarr=[]
for ele in lrres:
    lrresarr.append(ele[0])

sgres=aimstd.inverse_transform(sgres)#梯度下降结果
#这个结果因为之前标准化过了，预测出来的实际上不是真正的波士顿的房价，是标准化之后的房价
# 所以要把标准化的结果重新转换成正常的价格
print("LinearRegression res is %s" %lrresarr)
print("SGDRegressor res is %s" %sgres)
print("linear regression %s" %len(lrresarr))
print("SGDRegressor %s" %len(sgres))
print("aim_train %")
lrmean=mean_squared_error(aim_test,lrresarr)
print(lrmean)
sdgmean=mean_squared_error(aim_test,sgres)
print(sdgmean)
#测试岭回归
rd=Ridge(alpha=0.1)
rd.fit(data_trainstd,aim_trainstd)
rdres=rd.predict(data_teststd)
rdres=aimstd.inverse_transform(rdres)
print("rdres %s" %rdres)
rdmean=mean_squared_error(aim_test,rdres)
print(rdmean)

joblib.dump(rd,"rd.pkl")#记录rd（岭回归）已经计算的模型
rd2=joblib.load("rd.pkl")
print("rd conf:%s" %rd.coef_)
print("rd2 conf:%s" %rd2.coef_)
print("rd2 equals rd1 is : %s" %(numpy.array(rd)==numpy.array(rd2)))










