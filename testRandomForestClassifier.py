#coding:utf-8
import pandas,numpy
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.model_selection import GridSearchCV#引入网格搜索匹配最优参数
#要注意我们的这个文件是gbk格式的
dataset=pandas.read_csv("titanic.csv",encoding="GBK",sep=",")
aimdata=dataset["survived"]
dataset=dataset[["pclass","age","sex","floor"]]
#因为里面有很多还是NAN的所以我们只获取部分数据，懒得处理其他的
#对于年龄为空的我们进行预处理
dataset["age"].fillna(dataset.loc[:,"age"].mean(),inplace=True)

dv=DictVectorizer(sparse=True)#因为里面有很多字符串形式的数据，所以我们要对他进行字符串处理
#把字符串类型的直接转换成数字类型的
testalldata=dv.fit_transform(dataset.to_dict(orient="records"))#orient目的是为了让他一条一条转换
print(dv.get_feature_names())#获取数据转换之后的表头
data_train,data_test,aim_train,aim_test=train_test_split(testalldata,aimdata,test_size=0.2)
rc=RandomForestClassifier()
#对于随机森林使用网格搜索配置参数，有可能运行结果的时候会非常的慢
#max_features="auto"每个决策树的最大特征数
#gc=GridSearchCV(rc,{"n_estimators":[200,300,400,500],"criterion":["gini"],"max_depth":[5,20,15],"bootstrap":[True],"max_features":["auto"]},cv=5)#用20次交叉验证，提高准确性
gc=GridSearchCV(rc,{"n_estimators":[300],"criterion":["gini"],"max_depth":[20],"bootstrap":[True],"max_features":["auto"]})
gc.fit(data_train,aim_train)
print(gc.best_params_)
pre=gc.predict(data_test)
score=gc.score(data_test,aim_test)
print(score)

mes={'age':28, 'floor':1, 'pclass=1st':0, 'pclass=2nd':1, 'pclass=3rd':0, 'sex=female':0, 'sex=male':1}

#print(type(data_test))
mespan=pandas.DataFrame(mes,index=[0])
print(mes)
print(gc.predict(mespan))



