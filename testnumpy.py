import numpy
# a=numpy.arange(12)
# print(a.dtype)
# print(a)
# print(a.shape)
# b=a.reshape((3,4))
# print(b)
# print(b.shape)
# # print(a)
# # c=a.reshape((2,2,3))
# # print(c)
# # print(b[0])
# d=b.shape[0]
# print(d)
a=numpy.arange(0,12).reshape(2,6)
print(a)
b=numpy.arange(12,24).reshape(2,6)
print(b)
print("竖直拼接")
d=numpy.vstack((a,b))
print(numpy.vstack((a,b)))#竖直拼接
print("水平拼接")
print(numpy.hstack((a,b)))#水平拼接
print(a.max())
print(numpy.median(a,axis=0))#求a的h行平均值
print(a.dtype)
c=a.astype("float")
print(c.dtype)
print(a.std(axis=1))#求a的列标准差
print(a[1,:2])#获取第二行，前两个数
print(a[1,3:])#获取第二行后三个数
print(numpy.ptp(a))#获取a的极值
e=numpy.vsplit(d,4)
print(e)
f=numpy.hsplit(d,3)
print(f)

print(numpy.argmax(a,axis=0))