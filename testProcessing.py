from  sklearn.preprocessing import MinMaxScaler,StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.decomposition import PCA
# mm=MinMaxScaler(feature_range=(2,4))
a=(range(1,5),range(5,13)[::2],range(9,13))
# print(a)
# resmm=mm.fit_transform(a)
# print(resmm)
# st=StandardScaler()
# d=st.fit_transform(a)
# print(d)
vh=VarianceThreshold(threshold=12)
print(vh.fit_transform(a))
pc=PCA(n_components=0.95)
print(pc.fit_transform(a))