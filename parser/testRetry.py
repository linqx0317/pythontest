#coding：utf-8
#爬虫失败重试retry模块尝试调用
import requests


import retrying

@retrying.retry(stop_max_attempt_number=2)
def  _doParser(url,method,timeout,data,headers,proxies,verify,cookie,allow_redirects):
    # retrying _stop_max_attempt_number如果调用不成功允许重试，最多两次
    res=None
    if method=="get":#这些参数实际上都在requests里面的Request
        res=requests.get(url,timeout=timeout,headers=headers,proxies=proxies,verify=verify,cookies=cookie,allow_redirects=allow_redirects)
    else:
        res = requests.post(url,data=data,timeout=timeout,headers=headers,proxies=proxies,verify=verify,cookies=cookie,allow_redirects=allow_redirects)
    return res

def doParser(url,method="get",timeout=10,data={},headers={},proxies={},verify=True,cookie={},allow_redirects=False):
    try:
        res=_doParser(url,method=method,timeout=timeout,data=data,headers=headers,proxies=proxies,verify=verify,cookie=cookie,allow_redirects=allow_redirects)
        return res
    except Exception as e:
        print(e)

if __name__ == '__main__':
    #开始获取初始cookie
    url="https://passport.isoftstone.com/"
    user="qxlina"
    passwd="Aa911453794"
    header=dict()
    header["Accept"]="text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
    header["Accept-Encoding"]="gzip, deflate, br"
    header["Accept-Language"]="zh-CN,zh;q=0.9"
    header["Cache-Control"]= "max-age=0"
    header["Connection"]= "keep-alive"
    header["Host"]="passport.isoftstone.com"
    header["sec-ch-ua"]='"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"'
    header["sec-ch-ua-mobile"]= "?0"
    header["Sec-Fetch-Dest"]= "document"
    header["Sec-Fetch-Mode"]= "navigate"
    header["Sec-Fetch-Site"]= "none"
    header["Sec-Fetch-User"]= "?1"
    header["Upgrade-Insecure-Requests"]="1"
    header["User-Agent"]="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36"
    res=doParser(url,method="get",timeout=10,headers=header,verify=False)
    cookie=res.cookies
    cookiedict= requests.utils.dict_from_cookiejar(cookie)
    # print(res.content.decode())
    data={
        "emp_DomainName": "qxlina",
        "mp_Password": "Aa911453794",
        "DomainUrl":None,
        "returnUrl":None,
        "p":None,
        "t": None
    }
    header["Sec-Fetch-Site"] = "same-origin"
    header["Content-Type"]= "application/x-www-form-urlencoded"
    header["Origin"]= "https://passport.isoftstone.com"
    header["Referer"]="https://passport.isoftstone.com/"
    res = doParser(url,method="post",timeout=30,data=data,headers=header,cookie=cookiedict,verify=False,allow_redirects=True)
    cookie = res.cookies
    cookiedict = requests.utils.dict_from_cookiejar(cookie)
    hea=res.headers
    url=res.url
    print(res.content.decode())
    if res.is_redirect==True:
        pass








