#coding:utf-8
import pandas,numpy
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV#引入网格搜索匹配最优参数
from  sklearn.tree  import DecisionTreeClassifier,export_graphviz#引入决策树
from sklearn.feature_extraction import DictVectorizer
#要注意我们的这个文件是gbk格式的
dataset=pandas.read_csv("titanic.csv",encoding="GBK",sep=",")
#我们来获取训练集的特征数据
testalldata=dataset[["pclass","age","sex","floor"]]
#经过数据监测，age空的时候直接可以通过isnulll获取到
#这里我们用age的平均年龄来对空值进行替换，这里一定要inplace，不然testalldata的age数目不会发生变化
testalldata["age"].fillna(testalldata.loc[:,"age"].mean(),inplace=True)
#age替换了之后我们重新查找是否还有年龄为空的人，并且输出监测结果
datanull=testalldata[(testalldata.loc[:,"age"]=="NA")|(testalldata.loc[:,"age"]==numpy.NaN)|(testalldata.loc[:,"age"].isnull())]
print(datanull)
#查看下年龄的平均值
print(testalldata.loc[:,"age"].mean())
#获取目标值
aimdata=dataset["survived"]
#print(aimdata)#这个里面实际上存活的就是1，死亡的就是0，不用进行转换了
print(testalldata.head(5))
dv=DictVectorizer(sparse=True)#因为里面有很多字符串形式的数据，所以我们要对他进行字符串处理
#把字符串类型的直接转换成数字类型的
testalldata=dv.fit_transform(testalldata.to_dict(orient="records"))#orient目的是为了让他一条一条转换
#print(testalldata)
print(dv.get_feature_names())#获取数据转换之后的表头
data_train,data_test,aim_train,aim_test=train_test_split(testalldata,aimdata,test_size=0.2)
#进行训练集与测试集的区分
#使用gini系数的方式来画一个5层深度的决策树
treeDecision=DecisionTreeClassifier( criterion="gini",max_depth=5)
treeDecision.fit(data_train,aim_train)#决策树的使用比较简单只要把测试集和测试目标放进去
#然后我们就可以预测准确率了
treeDecision.predict(data_test)
scoreres=treeDecision.score(data_test,aim_test)
print(scoreres)
#绘制预测结果
ep=export_graphviz(treeDecision,out_file="treeDecision.dot")
#这个图片我们可以直接利用graphviz软件直接看，如果是在window条件下
