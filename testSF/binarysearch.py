#coding=utf-8
"""自己写二分查找 """
def binarysearchfun(list,num):
    n=len(list)
    if not list or n==0:
        return False,0
    min = 0
    max = n - 1
    while min<=max:
        mid = (max + min) // 2
        if list[mid]!=num:
            if list[mid]<num:
               min=mid+1
            else:
                max=mid-1

        else:
            return True,mid
    return False, 0


if __name__ == '__main__':
    searchlist=range(0,11)
    while True:
        searchnum=eval(input("pls input a num"))#eval 自动类型转换
        if searchnum!=100:
            bolres,posi=binarysearchfun(searchlist,searchnum)
            if bolres:
                print("找到了%s 位置是%s" %(searchnum,posi))
            else:
                print("没找到%s" %searchnum)
        else:
            exit(0)