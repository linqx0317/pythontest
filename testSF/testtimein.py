#coding:utf-8
import timeit
"""用来计算代码运行的平均时间，时间复杂度相关"""
# timeit.timeit(str=)
def testfun1():
    li1=[]
    for i in range(10000):
        li1+=[i]
def testfun2():
    li2=[i for i in range(10000)]
def testfun3():
    li3=[]
    for i in range(10000):
        li3 = li3+[i]
    #print("li3 len is:%s" %len(li3))
time1=timeit.Timer(stmt="testfun1()", setup="from __main__ import testfun1")
time1res=time1.timeit(10000)
print("time1res:%s" %time1res)
time2=timeit.Timer(stmt="testfun2()", setup="from __main__ import testfun2")
time2res=time2.timeit(10000)
print("time2res:%s" %time2res)
#setup里面的内容实际上就是引入本py文件里面的testfun3这个方法
time3=timeit.Timer(stmt="testfun3()", setup="from __main__ import testfun3")
time3res=time3.timeit(1000)
print("time3res:%s" %time3res)



