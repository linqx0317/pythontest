#coding=utf-8
"""自己手写二叉树"""
class binarytreenode():
    def __init__(self,obj):
        """创建用来记录二叉树的每个子节点的左右子节点和数值
        :param obj: 节点数值
        """
        self.left=None
        self.right=None
        self.val=obj
class binarytree():
    def __init__(self):
        self.root=None
    def add(self, obj):
        queue=[]
        node=binarytreenode(obj)
        if not self.root:
            self.root=node
            return
        else:
            queue.append(self.root)
            #只要创建了队列，那么就一定不会为空，所以要根据队列的长度进行判断
            while  len(queue)!=0:
                #如果队列里面有内容，那么就开始比对，当前待放入的数据比根节点大还是小
                #如果是小于等于，就获取左节点，放入队列等待下一次比对
                #如果获取不到左节点，那么当前这个数就会变成左节点
                #如果是大于，那么就获取右节点，放入队列等待下一次对比来判断
                #如果获取不到节点，那么当前这个数就会变成右节点
                res=queue.pop()
                val=res.val
                if obj<=res.val:
                    if res.left!=None:
                        queue.append(res.left)
                    else:
                        res.left=node
                        break
                elif obj>res.val:
                    if res.right!=None:
                        queue.append(res.right)
                    else:
                        res.right=node
                        break
    def getBreadth(self):
        """获取广度遍历的内容，一行行从左往右读取"""
        queue=[]#用来记录待取子节点的节点
        resqueue=[]#用来记录待返回的内容
        if  self.root!=None:
            queue.append(self.root)
        else:
            return
        while len(queue)!=0:
            res=queue.pop(0)
            resval=res.val
            resqueue.append(res.val)
            if res.left!=None:
                queue.append(res.left)
            if res.right!=None:
                queue.append(res.right)
        return  resqueue
    def getPreorder(self,root,queue):
        """先序遍历，根左右，采用迭代来获取"""
        if root!=None:
            queue.append(root.val)
        if root.left!=None:
            self.getPreorder(root.left,queue)
        if root.right!=None:
            self.getPreorder(root.right,queue)
        return queue
    def getInorder(self,root,queue):
        """中序排序，左中右"""
        if root!=None:
            if root.left!=None:
                self.getInorder(root.left,queue)
            queue.append(root.val)
            if root.right!=None:
                self.getInorder(root.right,queue)
        return queue
    def getPostorder(self,root,queue):
        """获取后序遍历的结果，左右中"""
        if root!=None:
            if root.left!=None:
                self.getPostorder(root.left,queue)
            if root.right!=None:
                self.getPostorder(root.right,queue)

            queue.append(root.val)
        return queue


if __name__ == '__main__':
    bt=binarytree()
    # for i in range(10):
    #     bt.add(i)
    bt.add(15)
    bt.add(8)
    bt.add(20)
    bt.add(19)
    bt.add(6)
    bt.add(5)
    bt.add(10)
    bt.add(7)
    bt.add(13)
    bt.add(9)
    bt.add(25)
    # lambda i :bt.add(i)
    #广度排序
    resbrth=bt.getBreadth()
    print("breadth traversal res is :%s" %resbrth)
    #breadth traversal res is :[15, 8, 20, 6, 10, 19, 25, 5, 7, 9, 13]
    #先序排序
    respro=bt.getPreorder(bt.root,[])
    print("preoder travelsal res is:%s" %respro)
    #preoder travelsal res is:[15, 8, 6, 5, 7, 10, 9, 13, 20, 19, 25]
    #中序遍历
    resind=bt.getInorder(bt.root,[])
    print("inorder travelsal res is:%s" %resind)
    #inorder travelsal res is:[5, 6, 7, 8, 9, 10, 13, 15, 19, 20, 25]
    #后续遍历
    respost=bt.getPostorder(bt.root,[])
    print("postorder travelsal res is:%s" %respost)
    #postorder travelsal res is:[5, 7, 6, 9, 13, 10, 8, 19, 25, 20, 15]