#coding=utf-8
class bnode():
    def __init__(self,val):
        self.next=None#用来记录后一个节点
        self.val=val
        self.preSon=None
        self.nextSon = None

class singlenode():
    """作为b树的每个大节点，这里的内容要进行排序插入不能随手插入"""
    def __init__(self):
        """每个节点都记录他的父节点和头结点的位置"""
        self.head=None
        self.leftfather = None
        self.nextfather = None
        self.fatherList=None#记录父节点所在的位置
    def addsinglenode(self,val):
        """单节点增加排序内容"""
        node = bnode(val)
        if self.head == None:
            self.head = node
        else:
            curnode=self.head
            exnode=None
            while curnode!=None :
                if curnode.val<val :
                    exnode=curnode
                    if curnode.next!=None:
                        curnode=curnode.next
                    else:
                         curnode.next=node
                         break
                else:
                    node.next = curnode
                    if exnode!=None:
                        exnode.next = node
                    else:
                        self.head=node
                    break

    def addbynode(self,node):
        """根据节点添加，直接输入一个bnode类型"""
        if self.head == None:
            self.head = node
        else:
            curnode=self.head
            exnode=None
            while curnode!=None :
                if curnode.val<node.val :
                    exnode=curnode
                    if curnode.next!=None:
                        curnode=curnode.next
                    else:
                         curnode.next=node
                         break
                else:
                    node.next = curnode
                    if exnode!=None:
                        exnode.next = node
                    else:
                        self.head=node
                    break

    def getbyIndex(self,index):
        if self.head!=None:
            i=0
            cornode=self.head
            while cornode!=None and i!=index:
                i+=1
                cornode=cornode.next
            if i==index:
                return cornode
        return

    def searchRange(self,val):
        """查找比某个数刚好小的某个数,找到了就返回节点,找不到就返回None，
        也就这个待查找的数刚好在头结点上
        """
        curnode=self.head
        exnode=None
        while curnode!=None:
            if curnode.val<=val:
                exnode = curnode
                curnode=curnode.next
            else:
                return exnode
        return exnode




    def getlen(self):
        """获取单个节点上长度"""
        curnode=self.head
        i=0
        while curnode!=None:
            i+=1
            curnode=curnode.next
        return i

class btree():
    def __init__(self,degree):
        """自己手写一个b树，包括增删查的操作，为了操作简便，我将每个节点都设置为一个节点的链表"""
        self.degree=int(degree)#用来设置树的度
        self.head=None
    def add(self,val):
        """b数元素的添加"""
        if self.head==None: #如果本身是一个空的b树，实际上添加一个节点直接添加就行了
            si=singlenode();
            si.addsinglenode(val)
            self.head=si
        else:#本身不是空的b树的情况
            curnode=self.head
            self.addnode(val, curnode)
    def search(self,val):
        """查找数据是否在当前的b树里面"""
        if self.head==None:
            return False
        else:
            curnode=self.head
            while curnode!=None:
                ressearch=curnode.searchRange()
                if ressearch.val==val:
                    return True
                else:
                    if ressearch==None and curnode.head.preSon!=None:
                        curnode=curnode.head.preSon
                    elif ressearch!=None and ressearch.nextSon!=None:
                        curnode=ressearch.nextSon
            return False
    def addnode(self,val,curnode):
        """非主节点的情况下b树添加节点,返回最后插入，最上层的节点"""
        while curnode != None:
            exnode = curnode.searchRange(val)#先查找小于等于当前插入值的左前节点
            if exnode.val == val:
                print("data already exist no need input")
            if exnode == None:  # 如果找不到小于当前值的左前节点，就说明小于头结点
                if curnode.head != None:#获取头结点，接着向左下查找
                    if curnode.head.preSon != None:
                        #如果当前节点没有左前节点，就直接插入，如果有接着寻找
                        curnode = curnode.head.preSon
                    else:#找到要插入的叶子节点
                       elenode=bnode(val)
                       curnode=self.addorSplit(elenode, curnode)
                       return curnode
            else:#如果获取的有右节点，那么也是向下找叶子节点
                if exnode.nextSon != None:
                    #如果当前的查找的节点的右叶子不为空，那么就接着向下超找
                    curnode = exnode.nextSon
                else:#找到要插入的叶子节点就开始进行插入
                    elenode = bnode(val)#将它封装进小的节点
                    curnode =self.addorSplit(elenode, curnode)
                    return curnode

    def addorSplit(self,elenode,curnode):
        """开始判断是插入当前list节点，还是当前list节点开始分裂
        :param elenode:List里的小节点，待插入curnode curnode：List作为的节点
        """
        if curnode.getlen() + 1 < self.degree:
            # 如果当前节点数量还没有大于阶-1，那么直接插入
            curnode.addbynode(elenode)
            return
        else:  # 如果当前节点大于度，那么开始分裂
            curnode.addbynode(elenode)#先插入
            lencur=curnode.getlen()#获取插入之后的总长
            print("begin split current length is:%s" %lencur)
            cutindex=lencur//2#获取中间节点
            if lencur%2==0:#链表从0的下标位置开始，所以如果是偶数需要减掉一位数
                cutindex-=1
            mid=curnode.getbyIndex(cutindex)#获取之后的父节点
            nodebeforemid = curnode.getbyIndex(cutindex - 1)
            nodelatermid = curnode.getbyIndex(cutindex +1)
            sitmp = singlenode()
            sitmp.addbynode(nodelatermid)
            mid.next = None
            nodebeforemid.next = None
            mid.perSon = curnode
            mid.nextSon = sitmp
            sitmp.leftfather=mid
            if curnode.fatherList==None:
                #TODO
                # 插入的时候多了一个head
                simid = singlenode()
                simid.addbynode(mid)
                sitmp.fatherList=simid
                curnode.fatherList=simid
                if curnode==self.head:
                    self.head=simid
            else:
                sitmp.fatherList = curnode.fatherList
                self.addorSplit(mid,curnode.fatherList)

if __name__ == '__main__':
    btreeres=btree(5)
    btreeres.add(6)
    btreeres.add(15)
    btreeres.add(30)
    btreeres.add(70)
    btreeres.add(80)
    btreeres.add(100)
    btreeres.add(110)
    btreeres.add(120)
    btreeres.add(130)
    btreeres.search(130)
    print("abn")
