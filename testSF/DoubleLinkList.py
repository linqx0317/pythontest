#coding:utf-8

from testSF.singlenodeList import node,singlenodelistself

class DoubleNode(node):
    """模拟写双向链表，因为只是比单项链表多了点东西，所以直接拿单项链表直接来进行更改"""
    def __init__(self,val,next=None,pre=None):
        #继承与单链表的节点，多一个pre
        super().__init__(val)
        self.pre=pre
class DoubleLinkList(singlenodelistself):
    """
    因为双向链表和单向链表只是多保存了一个前节点，
    所以有很多方法与属性可以直接继承，
    只重写其中不一样的部分
    """
    def __init__(self):
        super().__init__()#他会自己把self传入
        # self.__head=None

    def add(self,val):
        """尾部增加,因为有个前节点位置记录所以这个方法需要重写"""
        doublenode = DoubleNode(val)
        if self.__head__==None:
            self.__head__=doublenode
        else:
            lastnode=self.getlastele()
            lastnode.next=doublenode
            doublenode.pre=lastnode
    def  inserthead(self,val):
        """头部插入数据"""
        doublenode = DoubleNode(val)
        if self.__head__==None:
            self.__head__=doublenode
        else:
            doublenode.next=self.__head__
            self.__head__.pre=doublenode
            self.__head__=doublenode
    def remove(self,i):
        """删除指定位置的元素"""
        cur=self.__getele__(i)

    def  insert(self,i,val):
        """指定位置插入数据"""
        doublenode = DoubleNode(val)
        if self.__head__==None or i==0:
            self.inserthead(val)
        else:
            cur=self.__getele__(i)
            if cur!=None:
                doublenode.next=cur
                doublenode.pre=cur.pre
                cur.pre.next=doublenode
                cur.pre=doublenode
            else:
                self.add(val)
if __name__ == '__main__':
    doublelink=DoubleLinkList()
    print("is  empty %s" %doublelink.isempty())
    for i in range(10):
        doublelink.add(i)
    doublelink.iter()
    print("get ele 5 value is :%s"  %doublelink.get(5))
    print("insert in head 123 %s" %doublelink.inserthead(123))
    doublelink.iter()
    """测试指定位置插入"""
    doublelink.insert(0,1145)
    doublelink.iter()
    doublelink.insert(100,100)
    doublelink.iter()
    doublelink.insert(5,111)
    doublelink.iter()
    print("search 124 in doublelink result is :%s"  %doublelink.search(124))
    print("search 123 in doublelink result is :%s" % doublelink.search(123))
    print("search 1145 in doublelink result is :%s" % doublelink.search(1145))
    print("search 100 in doublelink result is :%s" % doublelink.search(100))
