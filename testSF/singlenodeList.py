#coding:utf-8
#自定义单链表节点
class node:
    def __init__(self,val,next=None):
        """
            singlenode element
            :param
            val :input value
            next :next node
            """
        self.info=val
        self.next=next
#自定义一个单链表
class singlenodelistself:
    def __init__(self):
        #把它设置为内部私有
        self.__head__=None#如果写成__head子类也继承不了
    def isempty(self):
        if self.__head__==None:
            return True
        return False

    def add(self,val):
        """对尾添加"""
        nodeele=node(val)
        if self.__head__ ==None:
            self.__head__=nodeele
        else:
            lastnode=self.getlastele()
            lastnode.next=nodeele

    def getlastele(self):
            """获取最后一个元素"""
            lastnode=None
            if self.__head__ == None:
                return None
            else:
                next = self.__head__
                while next.next != None :
                    next = next.next
                return next

    def inserthead(self,val):
        """头位置插入"""
        if self.__head__==None:
            self.__head__=node(val)
        else:
            nodetmp=node(val,self.__head__)
            self.__head__=nodetmp

    def length(self):
        """获取单链表的长度"""
        i=0
        nodetmp=self.__head__
        while nodetmp!=None:
            i+=1
            nodetmp=nodetmp.next
        return i

    def insert(self,i,val):
        """
            :param i location val value
            :common 指定位置插入，第一个参数是插入到某个位置，
            """
        if i==0:
            self.inserthead(val)
        else:
            nodeEx=self.__getele__(i-1)
            print("ex is %s" %nodeEx.info)
            nodecur=self.__getele__(i)
            print("current is %s" %nodecur.info)
            nodetmp=node(val,nodecur)
            nodeEx.next=nodetmp
    def remove(self,rei):
        """根据下标删除数据"""
        i=0

        if  self.__head__==None:#如果单链表是 空的就没什么可以删除的
            return
        cur = self.__head__
        if rei==0 :#如果是删除首个的情况
            self.__head__=self.__head__.next
        else:
            while cur!=None and cur.next!=None:
                if i+1==rei:
                    cur.next=cur.next.next
                i+=1
                cur=cur.next

    def __getele__(self,i):
        """获取某个为位置的节点
       :return node
        """
        itmp=0
        if self.__head__==None:
            return None
        else:
            next=self.__head__
            while next!=None and i!=itmp:
                next=next.next
                itmp +=1
            return next

    def search(self,serval):
        """查找某个数是否在这个单链表里面"""
        cur=self.__head__
        while cur!=None:
                if cur.info==serval:
                    return True
                cur=cur.next
        return False
    #获取指定地点的值
    #获取小于0就返回第一个head，如果超过长度就返回最后一个节点
    def get(self,i):
        itmp=0
        if self.__head__==None:
            return None
        else:
            next=self.__head__
            while next!=None and i!=itmp:
                next=next.next
                itmp +=1
            return next.info
    #遍历里面的内容
    def iter(self):
        if self.__head__==None:
            return
        else:
            next=self.__head__
            putout=''
            while next!=None:
                # val = next.info
                putout+=str(next.info)+" "
                next=next.next
            print(str(putout))



if __name__ == '__main__':
    singlelist=singlenodelistself()
    print("is empty:%s" %singlelist.isempty())
    print("current length is:%s " %singlelist.length())
    for i in range(10):
        singlelist.add(i)
    print("current length is:%s " % singlelist.length())
    singlelist.iter()
    locationstr=3
    print("print %s ele:%s" %(locationstr,singlelist.get(locationstr)))
    print("insert head" )
    singlelist.inserthead(111)
    singlelist.iter()
    print("current length is:%s " % singlelist.length())
    singlelist.insert(4,100)
    singlelist.iter()
    serval=111
    print("whether %s in  singlelist:%s" %(serval,singlelist.search(serval)))
    serval = 9
    print("whether %s in  singlelist:%s" % (serval, singlelist.search(serval)))
    serval = 110
    print("whether %s in  singlelist:%s" % (serval, singlelist.search(serval)))
    singlelist.remove(0)
    singlelist.iter()
    singlelist.remove(singlelist.length()-1)
    singlelist.iter()
    singlelist.remove(3)
    singlelist.iter()
