from matplotlib import  pyplot as plt,font_manager
x=range(2,26,2)#最后这个是步长，我们可以自己设置
y=[15,16,17,18,19,11,12,13,15,18,10,12]
y1=[20,22,14,16,17,13,12,14,16,12,21,10]
fig=plt.figure(figsize=(6,4),dpi=90)
#设置matplotlib图片的大小，要注意在图片开始赋值之前设置
plt.plot(x,y,label="2020年6月20日",color="pink",linestyle=':')#给matplotlib的图表赋值，显示具体的内容
plt.plot(x,y1,label="2020年6月21日")
#给matplotlib的图表赋值，显示具体的内容，这里是给出了多根折线，并且设置了显示的颜色以及折线样式
myfont=font_manager.FontProperties(fname="./static/STSONG.TTF")
#显示图表中支持显示中文，这里获取font的文件，然后创建自定义font对象
# 然后在进行设置的时候把文字类型设置为我们自定义的Font对象，不设置的话中文可能不显示
plt.xticks(x,[str(i)+"点" for i in x],rotation=45,font_properties=myfont)#设置x轴的间距，这里不能使用统计不出长度的int类型
#font_properties设置文本格式，rotation表示文字转多少度
plt.yticks(range(min(y)-2,max(y)+4,2),[str(i)+"点" for i in range(min(y)-2,max(y)+4,2)],font_properties=myfont)#设置y的间距
#第一个参数是y的刻度，第二个参数是我们设置刻度显示的 内容
plt.xlabel("时间",fontproperties=myfont)#设置x轴描述信息，x轴是干什么的
plt.grid(alpha=0.5)#显示网格，alpha是线条透明度设置
plt.ylabel("温度 单位:(度)",fontproperties=myfont)#设置y轴描述信息
plt.title("一天温度变化",fontproperties=myfont)#设置这张图的标题
plt.legend(prop=myfont,loc=1)#prop设置字体支持显示中文，loc表示这个图例的位置
plt.savefig("./static/testpic.svg")
plt.show()#显示matplotlib图表