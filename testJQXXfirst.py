from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
import jieba
# #调用scikit-lear
# dv=DictVectorizer(sparse=True)
# shuju=[{"name":"linqx","age":21,"tel":"123","email":"911"},{"name":"linqx01","age":22,"tel":"124","email":"912"},{"name":"linqx03","age":23, "tel":"125"}]
# print(dv.fit_transform(shuju).toarray())
# print(dv.feature_names_)
# cv=CountVectorizer()#做中文分词操作，首先实例化一个对象
# a="虽然 Python 的处理速度不是特别快，但 Python 的 dict 优点是使用方便，稀疏（不需要存储的特征），并且除了值之外还存储特征名称。"
# b="该描述可以被矢量化为适合于呈递分类器的稀疏二维矩阵（可能在被管道 text.TfidfTransformer 进行归一化之后）:"
# cona=jieba.cut(a)
# conb=jieba.cut(b)
# alist=list(cona)
# blist=list(conb)
# print(cona)
# print(blist)
# print(alist)
# a=" ".join(alist)
# b=" ".join(blist)
# cv=CountVectorizer()
# print(cv.fit_transform([a,b]).toarray())
# print(cv.get_feature_names())
tf=TfidfVectorizer()
c="虽然 Python 的处理速度不是特别快，但 Python 的 dict 优点是使用方便，稀疏（不需要存储的特征），并且除了值之外还存储特征名称。"
d="该描述可以被矢量化为适合于呈递分类器的稀疏二维矩阵（可能在被管道 text.TfidfTransformer 进行归一化之后）:"
tff=tf.fit_transform([c,d])
print(tff.toarray())
print(tf.get_feature_names())
