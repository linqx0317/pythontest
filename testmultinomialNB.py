from sklearn.datasets  import fetch_20newsgroups
from sklearn.model_selection import train_test_split,GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import  TfidfVectorizer
from sklearn.metrics import classification_report

#进行字段占比统计
newsorginaldata=fetch_20newsgroups(subset='all')
#加载新闻里面的所有数据
# print("*"*20+"to see what in data"+"*"*20)
# print(newsorginaldata.data[:5])#注意这个data是个list的格式
# print("*"*20+"to see what in DESCR"+"*"*20)
# print(newsorginaldata.DESCR)
# print("*"*20+"to see what in target"+"*"*20)
# print(newsorginaldata.target)
# print("*"*20+"to see what in target_names"+"*"*20)
# print(newsorginaldata.target_names)
tfi=TfidfVectorizer()
data_train,data_test,aim_train,aim_test=train_test_split(newsorginaldata.data,newsorginaldata.target,test_size=0.2)
#我们遇到spharse格式的数据的时候不是很好看为了方便观察我们可以.toarray()
data_train=tfi.fit_transform(data_train)#不可以提前统计，因为这个里面是文字
data_test=tfi.transform(data_test)#这里要注意
mb=MultinomialNB(alpha=1.0)#朴素贝叶斯
mb.fit(data_train,aim_train)
mb_predit=mb.predict(data_test)
print(mb_predit)
print("score is %s" %mb.score(data_test,aim_test))
# res=classification_report(aim_test,mb_predit,target_names=newsorginaldata.target_names)
# print(res)
gv=GridSearchCV(mb,{"alpha":[0.1,0.15,0.2,0.25,0.3]})#网格搜索
gv.fit(data_train,aim_train)
scoreres=gv.score(data_train,aim_train)
be=gv.best_estimator_#最好的参数模型
print(be)
cr=gv.cv_results_#每个超参数每次验证的结果
print(cr)


