# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import os,re,sys
import scrapy
from scrapy.cmdline import execute
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class IpsaspiderPipeline:
    def process_item(self, item, spider):
        print(item)
        return item


if __name__ == '__main__':
    #用来右键debug的相关配置
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    execute(["scrapy","crawl","ipsatest"])