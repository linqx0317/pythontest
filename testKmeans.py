#coding=utf-8
from sklearn.cluster import KMeans
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score#轮廓系数评估k聚类效果
from sklearn.decomposition import PCA
import pandas
from scipy.sparse import csc_matrix
import numpy
from matplotlib  import pyplot as py #尝试来绘制散点图
orders=pandas.read_csv("./data/orders.csv")
order_products__train=pandas.read_csv("./data/order_products__train.csv")
products=pandas.read_csv("./data/products.csv")
detail=orders.merge(order_products__train,on="order_id",how="left")
detail=detail.merge(products,on="product_id",how="left")
realusedata=detail[:500]
#一定要把这个结果进行转换成dict或者map才能进行测试
#{'dict', 'list', 'series', 'split', 'records', 'index'}
dv=DictVectorizer(sparse=True)#用来处理文字
dvres=dv.fit_transform(realusedata.to_dict(orient='records'))#要注意放进去的一定是可以遍历的dict或者map类型
#将DictVectorizer处理结果重新转化为pandas的DataFrame并且给定表头名称
pares=pandas.DataFrame.from_records(dvres.toarray(),columns=dv.feature_names_)
#print(pares)
for ele in pares.columns:
    #将处理完的数据进行空值替换，替换成均值，如果没有均值就替换为0
  try:
      meanval=pares[ele].mean()
      meanval=meanval if meanval!=numpy.NaN else 0
      #print(" %s is %s" %(ele,meanval))#查看平均值，这里太多了先不看了
      pares[ele].fillna(meanval,inplace=True)#不写inplace原来的Nan还是Nan
  except Exception as e:
      print("%s type is :%s" %(ele,type(detail[ele])))
#print(pares)#查看替换之后是否含有NAN，但是太多了可以不看
pca=PCA(n_components=0.9)
#保留百分之90的信息量，因为有可能含有信息太多
pcares=pca.fit_transform(pares)
#使用pca的时候先要对文字进行处理，转化为数据类型的
#这里一定要输入panda的数据类型，直接输入sparese类型的会直接报错
#print(pares)
#进行聚类，使用它默认的n_clusters8个类进行分类
kmeans=KMeans(n_clusters=3)#调用聚类分析的时候要把NAN的先给处理掉不然会提示错误
kmeans.fit(pcares)
#这里也可以直接用fit，直接训练不查看运行结果
# print(kmeanres[:1])#如果使用fit_transform的话就可以看分了几个类里面的具体内容是多少
# print(kmeanres.shape)
preditres=kmeans.predict(pcares)
print(preditres)
sires=silhouette_score(pares,preditres)#录入特征值和计算结果
print(sires)#获取轮廓系数，判断分类是否合理

