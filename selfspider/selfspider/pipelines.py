# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.cmdline import execute
import os
import sys
import logging
import datetime
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
logger=logging.getLogger(__name__)
dateday=datetime.datetime.now().date()
datedaystr=dateday.strftime("%Y%m%d")


class SelfspiderPipeline:
    def process_item(self, item, spider):
        return item
class LawsonspiderPipeline:
    def process_item(self, item, spider):
        #这里item最好不要重新赋值，不然后面数据处理管道就会获取不到spider处理传过来的值
        if spider.name=='LawsonspiderSpider':
            logger.info("{}:{}".format(spider.name, item.keys()[0]))
        return item
class FamilyPipeline:

    def open_spider(self, start_requests):
        #相关文件只打印一次，用来全局使用
        # print(start_requests.name)
        print(start_requests.name)
        #获取输出路径
        set=start_requests.settings.attributes.get("SAVE_FILE").value
        #打开输出文件
        self.familymartfile=open(start_requests.settings.attributes.get("SAVE_FILE").value+start_requests.name+".csv","w")
    def process_item(self, item, spider):
        if spider.name == 'familymartspider':
            # item.keys()这个出来是一个dict_list,不能直接get或者【0】获取所以干脆转化为list
            reskeys=list(item.keys())
            resvalue = list(item.values())
            logger.info("{}:{}".format(spider.name,resvalue[0]))
            #内容输出到文件
            self.familymartfile.write(",".join(str(i) for i in resvalue)+"\n" )
        return item
    def close_spider(self,spider):
        logger.info("{} finish begin to close save file".format(spider.name))
        self.familymartfile.flush()
        self.familymartfile.close()
        logger.info("{} finish  close save file {}".format(spider.name,self.familymartfile))




if __name__ == '__main__':
    #用来右键debug的相关配置
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    execute(["scrapy","crawl","familymartspider"])