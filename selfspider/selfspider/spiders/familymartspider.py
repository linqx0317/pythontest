import scrapy
from scrapy.cmdline import execute
import sys
import os
import copy
import re
import logging
logger=logging.getLogger(__name__)
class FamilymartspiderSpider(scrapy.Spider):
    name = 'familymartspider'
    allowed_domains = ['familymart.com.cn”']
    start_urls = ['http://www.familymart.com.cn/commodity/yingyangbiandang']

    def parse_detail(self,response):
        #获取每个分类的物品描述
        url_res=response.xpath("//div[@class=\"product-list\"]//div[@class=\"product-item\"]")
        for ele in url_res:
            item=response.meta
            con=ele.attrib.get("onclick")
            # con = response.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div")
            res=re.findall(r"clickPro(\(.*?\))",ele.xpath("./@onclick").extract_first())[0].replace("(","").replace(")","").split(",")

            item["pic"] = res[0]
            item["name"] = res[1]
            item["price"] = res[2]
            item["describe"] = res[3]

            #要注意输出的内容，不能直接输出str，所以这里随便包装成了一个dict文件
            logger.info("res is {}".format(res))
            yield item

    def parse(self, response):
        xpath_rule="/html//div[@class='subnav']//ul/li/a"
        xpath_res=response.xpath(xpath_rule)
        mainurl=self.settings.get("FAIMILY_MARTMAINURL")
        for ele in xpath_res:
            hrefstr=ele.xpath("./@href").extract_first()
            valuestr=ele.xpath("./text()").extract_first()
            item={"href":hrefstr,"value":valuestr}
            url=mainurl+hrefstr
            print(url)
            header={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"}
            yield scrapy.Request(url, headers=header,callback=self.parse_detail,meta=copy.deepcopy(item),dont_filter=True)




if __name__ == '__main__':
    #用来右键debug的相关配置
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    execute(["scrapy","crawl","familymartspider"])
