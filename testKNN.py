from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

#进行测试集与训练集的切分
from sklearn.neighbors import KNeighborsClassifier
#引入k近邻算法
iris=load_iris(as_frame=True)
#这个配置设置完了之后返回给我们的数据直接就是pandas的dataFrame类型,
# 不然返回的就是numpy的series类型
print(iris.feature_names)
print(iris.target_names)
print(iris.data.shape)#(150, 4)
print(iris.data.head(5))
#开始进行数据切分
data_train,data_test,aim_train,aim_test=train_test_split(iris.data,iris.target,test_size=0.25)
knn=KNeighborsClassifier(n_neighbors=5,algorithm='auto')
#不填n_neighbors默认就是5，lgorithm='auto'默认就是自己查询获取最优的计算方法
knn.fit(data_train,aim_train)
preditres=knn.predict(data_test)
#这个实际上计算出来的结果是目标里面结果的下标，个人认为
print("predit res is %s" %preditres)
score=knn.score(data_test,aim_test)
#这个实际上就是拿着训练集的数据集预测结果里面计算出来的结果下标与训练集的结果进行比对
#如果完全一致，那么就是1
print(aim_test.head(5))
print("score is %s" %score)



