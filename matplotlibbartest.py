#code=utf-8
from matplotlib import pyplot
from matplotlib import font_manager
filmname=["小红帽","灰太狼","佩奇","哈利波特 "]
harryport=[5,7,7,9,10]
redhat=[2,2,4,6,6]
pig=[12,14,15,20,21]
wolf=[21,12,4,1,5]
fonttype=font_manager.FontProperties(fname="./static/STSONG.TTF")
har=[i for i in range(1,20)[::4]]
red=[i+1 for i in range(1,20)[::4]]
pi=[i+2 for i in range(1,20)[::4]]
wo=[i+3 for i in range(1,20)[::4]]
pyplot.barh(har,harryport,label="哈利波特")#条形图注册
pyplot.barh(red,redhat,label="小红帽")#条形图注册
pyplot.barh(pi,pig,label="佩奇")#条形图注册
pyplot.barh(wo,wolf,label="灰太狼")#条形图注册

pyplot.xticks(range(25)[::4],["{}票房".format(i) for i in range(24)][::4],font_properties=fonttype)
pyplot.yticks([i for i in range(2,20)[::4]],["{}天".format((i+2)/4) for i in range(2,20)][::4],font_properties=fonttype)
pyplot.legend(prop=fonttype)

pyplot.savefig("./static/barpic.svg")
pyplot.show()
