from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing  import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import numpy
import pandas.io.formats.format as fmt
import pandas
import copy
colnames=["id","ct","cell size","cell shape","Marginal Adhesion","Single Epithelial Cell Size"
    ,"Bare Nuclei","Bland Chromatin","Normal Nucleoli","Mitoses","res"]
canorignialdata=pandas.read_csv("D:/Users/lin/PycharmProjects/shujufenxi/breast-cancer-wisconsin.data",names=colnames)
#这实际上可以写数据的网址，但是为了节约流量，手动将数据下载下来了
canorignialdata.replace("?",numpy.nan,inplace=True)
for ele in canorignialdata.columns:
    #如果数据里面含有numpy.nan,就把这个值直接替换为中位数
    if  not canorignialdata[canorignialdata[ele].isnull()==True].empty:
        print(ele)#查询含有Nan的列命
        #打印值含有NaN的
        print(canorignialdata[canorignialdata[ele].isnull() == True])
#         #如果这列不是str类型的这,直接判断会告诉你数据类型不一致的错误
#         #这里设定保留两位小数,注意这里处理不好的话可能反而会产生inf值
        #要注意这里转换为float，因为nan本身是float类型
        canorignialdata[ele]=canorignialdata[ele].astype(float)
#         #查看是否有空值
#         #print(canorignialdata[canorignialdata[ele].isnull()==True])
        meanval = canorignialdata[ele].mean()
        print(meanval)#查看平均值
#         #要注意我们不仅仅要处理NAN里面还有无穷的数据
#       #这里设定保留0位小数，实际上几位都行
        formatmean=format(meanval,".0f")
        print("formatmean is %s" %formatmean)
        canorignialdata[ele].fillna(meanval,inplace=True)
#print(canorignialdata.isnull().sum())#查询每一列为空的值的数量，目前可以看到没有为空值的
data_train,data_test,aim_train,aim_test=train_test_split(canorignialdata.iloc[:,1:10],canorignialdata.iloc[:,10:],test_size=0.2)
st=StandardScaler()
data_train=st.fit_transform(data_train)
data_test=st.transform(data_test)
lg=LogisticRegression(penalty='l2',C=1.0)
# penalty里面填写的l2实际上说的就是以12的形式正则化，
# C也是正则化的力度
lg.fit(data_train,aim_train)
res=lg.predict(data_test)
print("predict res is %s" %res)
#classification_report真实值，与测试
#clres=classification_report(aim_test,res)
clres=classification_report(aim_test,res,labels=[2,4],target_names=["良性","恶性"])
print(clres)
# scoreres=lg.score(res,aim_test)#这个测试还是有点问题
# print(scoreres)
#这里要记得将测试数据标准化，不然结果会错
resall=lg.predict(st.fit_transform(canorignialdata.iloc[:,1:10]))
npresall=numpy.array(resall)
print(len(resall))
print("res equal 2 is:%s" %len(npresall[npresall==2]))
print("res equal 4 is:%s" %(len(resall)-len(npresall[npresall==2])))













