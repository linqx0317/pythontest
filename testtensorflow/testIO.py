import tensorflow  as tf
import os
import sys
filename=os.listdir("./testtensorflow/testdata")
#print(filename)
filepathlist=[]#这里用list是因为tuple不支持增删
#print(os.getcwd()+"："+str(sys.path[0]))
#上面这两个都是获取路径的，获取出来都是正确的
for ele in filename:
    print(ele)
    # if "csv" in ele:
    filepathlist.append(os.getcwd() + "\\testtensorflow\\testdata\\" + str(ele))
    print(str(filepathlist))

#https://www.pythonheidong.com/blog/article/11355/
labeled_data_sets = []
datasetfile = tf.data.TextLineDataset(filepathlist)
print(type(datasetfile))
#v<class 'tensorflow.python.data.ops.readers.TextLineDatasetV1'
def printcon(x):
    print(x)
    return x
#TextLineDatasetV1实际上是DatasetV1Adapter的子类
#将 TextLineDatasetV1类型结果转换为 DatasetV1Adapter--实际就是dataset
dataset=datasetfile.map(lambda  x:printcon(x))
#这个lamba就相当于定义了一个简单的函数，将datasetfile里面的元素传入调用printcon，
#迭代整个文件将结果添加到自己的数据集
print(dataset)
print(type(dataset))#这里只是把数据读取出来了，但是并没有做切分
#labeled_data_sets.append(labeled_dataset)
# 定义迭代器,创建迭代器读取文件中的内容
iterator = datasetfile.make_one_shot_iterator()#遍历文档里面的每个元素
# 这里get_next()返回一个字符串类型的张量，代表文件中的一行。
x = iterator.get_next()#实际上感觉应该是个op
# for ele in dataset:
#     print(ele)
with tf.compat.v1.Session() as sess:
    for i in range(10):
        print(sess.run(x))




