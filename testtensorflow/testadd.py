import tensorflow as tf
print(tf.test.is_gpu_available())
print(tf.version)
a=tf.constant(5)
b=tf.constant(6)
selfg=tf.Graph()
with selfg.as_default():
    e=tf.constant(11)

with tf.Session(graph=selfg) as sess:
    #c=tf.add(a, b,name="add")
   # d=sess.run(c)#因为a和b正在默认的图里面，所以这里实际上获取不到a和b的值会直接报错
    #上面那个c虽然写了，但是没有实际执行，所以在这个run的地方报错了
    #print(d)#d作为结果没有graph
    print(e.graph)
    print(selfg)
    #print(c.graph)
    print(a.graph)
    print(sess.graph)



