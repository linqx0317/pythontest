#coding:utf-8
import tensorflow
import tensorflow as tf
#开始获取待测试数据
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(
    path="D:/Users/lin/PycharmProjects/shujufenxi/testtensorflow/testtensorflow/tensorboard/mnist.npz")
x_place = tf.placeholder(tf.float32, [None,28,28, 1])
y_place = tf.placeholder(tf.int32, [None, 10])

# 定义卷积层观察器的长宽，数量，图片的通道数量，以及观察器的数量
filters = [3,3,1,12]
# 定义步长，无论观察器上下左右移动，我们设置移动的长度都是一个像素
strides = [1, 1, 1, 1]
# 设置池化层观察器的窗口的大小
poolksize = [1, 2, 2, 1]  # 前后这两个1感觉是固定的，我们设置观察器的长宽是2
# 设置池化器的步长
poolstride = [1, 2, 2, 1]  # 一般我们采用两个步长


def pre():
    # x_place = tf.reshape(x_place, [-1, 28, 28, 1])
    # 自定义一个卷积层x, W, strides=[1, 1, 1, 1], padding="SAME"
    resconv = tensorflow.nn.conv2d(x_place, filters, strides, "SAME")
        # 自定义一个激活函数
    resrelu = tensorflow.nn.crelu(resconv)
        # 设置池化层，然后将他设置为自动零填充
    respool = tensorflow.nn.max_pool(resrelu, ksize=poolksize, strides=poolstride, padding="SAME")
            # 自定义一个卷积层
    resconv1 = tensorflow.nn.conv2d_transpose(value=respool, filter=filters, strides=strides, padding="SAME")
            # 自定义一个激活函数
    resrelu1 = tensorflow.nn.crelu(resconv1)
            # 设置池化层，然后将他设置为自动零填充
    respool2 = tensorflow.nn.max_pool(resrelu1, ksize=poolksize, strides=poolstride, padding="SAME")
        # 返回梯度下降的结果# 设置全连接层
        # 设置 权重w，这个w的形状是我们计算出来经过两次卷积之后的结果
    w = tf.Variable(tf.constant(1, shape=[7 * 7 * 12, 10]), trainable=True)
        # 设置偏置
    b = tf.Variable(tf.constant(0, shape=[10]), trainable=True)
    y_preres = tf.matmul(respool2, w) + b
    return y_preres


#数据太多所以我们只获取1000个数据进行计算
y_train=y_train[0:1000]
x_train=tf.constant(x_train[0:1000].reshape([-1,28,28,1]))
y_train=tf.one_hot(y_train,10,dtype=tf.int32)
sess = tf.compat.v1.Session()
init = tf.initialize_all_variables()

# 求平均交叉熵计算损失
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_place, pre()))
    # 设置梯度下降
train_op = tf.train.GradientDescentOptimizer(0.001).minimize(loss)
with tf.Session() as sess:
    sess.run(init)
    x_train = tf.data.Dataset.from_tensors(x_train)  # 返回类型dataset
    y_train = tf.data.Dataset.from_tensors(y_train)  # 返回类型为DatasetV1Adapter
    for i in range(5000):
        x_tmp = x_train.batch(50)
        y_tmp = y_train.batch(50)
        sess.run(train_op, feed_dict={ x_place: x_tmp, y_place: y_tmp})






