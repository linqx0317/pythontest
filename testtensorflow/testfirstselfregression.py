import tensorflow as tf
import time
import os
import tensorboard as tb
#创建正态分布的随机数
x_data=tf.random.normal([100,1],mean=20,stddev=20)#random_normal已经被替换成了random.normal
#进行矩阵的乘法运算,如果类型不一致不能进行数据乘法运算所以这里需要进行数据格式转换
y_true=tf.matmul(x_data,tf.cast([[8]],dtype=float))+19
w=tf.Variable([[20]],trainable=True,name="weight",dtype=float)
bais=tf.Variable(4,trainable=True,name="bais",dtype=float)

"""
Fetch argument <function global_variables_initializer at 0x00000290451470D0> has invalid type <class 'function'>, mu
st be a string or Tensor. (Can not convert a function into a Tensor or Operation.)
tensorflow 中sess.run不能直接运行普通常量，也就是这里的sess.run（2）
"""
#init_op=tf.initialize_all_variables()#初识化所有变量
y_predit=tf.matmul(x_data,w)+bais
loss=tf.reduce_mean(tf.square(y_true-y_predit))#这里是为了求均方误差
#前面这个填的是一个默认的权重
#这里的学习率设置太大可能会导致梯度爆炸设置的时候要考虑，一般都是小数点3-4位以上
train_op=tf.compat.v1.train.GradientDescentOptimizer(0.00001).minimize(loss)
#这个训练的操作节点也是需要运行的
init_op=tf.compat.v1.global_variables_initializer()#初识化所有变量
#saver=tf.compat.v1.train.Saver(max_to_keep=5,name="saverfirst")
saver=tf.compat.v1.train.Saver()
tf.app.flags.DEFINE_integer("max_train",1000,"train time")
flags=tf.app.flags.FLAGS
print("flags max_train is  %s" %(flags.max_train))
with tf.compat.v1.Session() as sess:
    print(sess.run(init_op)) #这个初识化一定要做而且要放在最前面，不然报错
    # 这个东西的返回值是None，没有返回值
    if os.path.exists("./testtensorflow/checkpoint"):
        #如果直接找./testtensorflow/checkpoint/firstcheckpoint这个他会找的是我们生成的文件，那么是肯定找不到的
        #因为文件名不完全一致，要确保一致才能重新获取到已经保存的权重与偏置
        #这个是加载原来已经计算好的sess
        saver.restore(sess, "./testtensorflow/checkpoint/firstcheckpoint")
        print("***yuanyou quanzhong is %s,op is %s" % (w.eval(), bais.eval()))
        #time.sleep(10)
    for i in range(flags.max_train):
        print("*"*10+"the "+str(i)+" time train begin")
        sess.run(train_op)
        print("quanzhong is %s,op is %s" % (w.eval(), bais.eval()))
        if i%5==0:
            saver.save(sess, "./testtensorflow/checkpoint/firstcheckpoint")
            time.sleep(10)

