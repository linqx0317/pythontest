#coding=utf-8
#简单神经网络，一层神经元
import tensorflow
import os
import tensorflow as tf

#用来保存我们已经就算好的结果
import numpy
#在最新的版本里面已经没有placeholder这个方法了
x_place=tf.placeholder(tf.float32,[None,784])
y_place=tf.placeholder(tf.int32,[None,10])
#进行one-hot编码转x_train换，
# 矩阵相乘是对应的行和列相乘的结果，所以需要第一个矩阵的列数与第二个矩阵的行数相等
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(
    path="D:/Users/lin/PycharmProjects/shujufenxi/testtensorflow/testtensorflow/tensorboard/mnist.npz")
y_train=y_train[0:1000]
y_train= tf.reshape(y_train,[1000,1])
y_train=tf.cast(y_train,tf.int32)
x_train=x_train[0:1000]
x_train = tf.cast(x_train, tf.int32)
x_train = tf.reshape(x_train, [1000, 784])
train=tensorflow.concat([x_train,y_train],axis=1)
# # y_train = tf.one_hot(y_train, 10)
# #train=tensorflow.cast(train[:,-1],dtype=tf.int32)
train=tensorflow.one_hot(train,[1000,785,10])#y展开了x相应的也应该展开
#这两种设定变量的方法都可以
w=tensorflow.Variable(tf.constant(1.0,shape=(784,10),dtype=tf.float32),trainable=True,)
# w=tensorflow.Variable(tf.ones([784,10]),trainable=True)#设置权重,我们的结果实际只有10个，0-9
bias=tf.compat.v1.Variable(tf.constant(0.0,shape=[10]),trainable=True)#设置偏置,我们的结果实际只有10个，0-9
y_predit=tf.matmul(x_place,w)+bias#设置预测结果
# y_predit=tf.matmul(x_place,w)
# 交叉熵损失，前面labels这里填写的是真实值，logits这里返回的是预测值
# 函数返回的是交叉熵损失
resloss=tf.nn.softmax_cross_entropy_with_logits(labels=y_place,logits=y_predit)
# #我们需要的是求平均的交叉熵损失
loss=tf.reduce_mean(resloss)
# #我们计算出来只是一个概率，我们把概率最大的变成1，然后看有多少是相等的，
# # 如果是相等的那么equal就会返回
equal_list = tf.equal(tf.argmax(y_place, 1), tf.argmax(y_predit, 1))
# 我们有n个样本
accuracy = tf.reduce_mean(tf.cast(equal_list, tf.float32))
#梯度下降
train_op = tf.train.GradientDescentOptimizer(0.001).minimize(loss)
#变量初始化
init_op=tensorflow.initialize_all_variables()
with tf.Session() as sess:
    #我们最直接把数据下载下来然后直接载入了
    sess.run(init_op)
    tmpsaver=tensorflow.compat.v1.train.Saver()
    pathdir="./testtensorflow/annminst"#后面这个半截annminst实际上是文件名
    if os.listdir(pathdir):
        tmpsaver.restore(sess,pathdir)
    for i in range(10):
        turn=1000/50
        for j in range(0,int(turn)):
            x_input=x_train[50*j:50*(j+1)]
            y_input=y_train[50*j:50*(j+1)]
            x_show=sess.run(x_input)
            y_show=sess.run(y_input)
            w_show=sess.run(w)
            print(str(sess.run(x_input[1])))
            sess.run(train_op,feed_dict={x_place:x_show,y_place:y_show})
            print("训练第%s步，w为：%s" % (i, sess.run(w[340])))
            print("训练第%s步，b为：%s" % (i, sess.run(bias[1])))
            print("训练第%s步，交叉熵为：%s" % (i, sess.run(resloss,feed_dict={x_place:x_show,y_place:y_show})))
            print("训练第%s步，损失为：%s" % (i, sess.run(loss,feed_dict={x_place:x_show,y_place:y_show})))
        tmpsaver.save(sess, pathdir)




