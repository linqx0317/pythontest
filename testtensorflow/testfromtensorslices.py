import tensorflow  as tf
import os
import tensorflow.contrib.eager as tfe
filename=os.listdir(os.getcwd() + "./testtensorflow/testdata")
#print(filename)
filepathlist=[]#这里用list是因为tuple不支持增删
for ele in filename:
    if "txt" in ele:
        filepathlist.append(os.getcwd() + "\\testtensorflow\\testdata\\" + str(ele))
column_namelist=["a","b","c","d"]#给定列名
dataset= tf.data.experimental.make_csv_dataset(filepathlist,4,header=False,column_names=column_namelist,field_delim=":",shuffle=False,num_epochs=6)
#dataset= tf.data.experimental.make_csv_dataset(filepathlist,4,header=False,column_names=column_namelist,field_delim=":",shuffle=False)
#header表示是否有列头，如是True就不读取第一行，shuffle是否洗牌
#num_epochs重复数据集里面所有数据的次数，重复完就结束了，并且如果还在调用获取数据会直接报错
#column_names表示读取的每一列叫啥，field_delim，根据什么来进行拆分，当中的3说的是一批次有多少个
print(type(dataset))
print(dataset)

labels = dataset.make_one_shot_iterator()# 第一个批次
con=labels.get_next()
counter=tf.Variable(0,tf.int32)#用来做计数器，变量，每次增加1
conres=tf.assign_add(counter,tf.constant(1))#自增
init_op=tf.initialize_all_variables()
with tf.Session() as sess:
    sess.run(init_op)#初识化变量
    print(type(con))
    while True:#只要执行sess.run(con)就会获取到下一个batch的数据
        print("*"*20+"get "+str(sess.run(conres))+" batch"+"*"*10)
        print(str(sess.run(con)))
        #经过测试，每个batch都会放在一个DIct里面


