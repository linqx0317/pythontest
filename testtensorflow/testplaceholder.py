import tensorflow
import numpy
a=tensorflow.constant(10)
plt=tensorflow.placeholder(tensorflow.float32,(None,3))#说明有3个特征值
#plt=tensorflow.placeholder(tensorflow.float32,(3,3))
with tensorflow.Session() as sess:
    res=sess.run(plt,feed_dict={plt:numpy.arange(1,10).reshape(3,3)})
    #但是在numpy里面没办法写成（None,3）的这种表达
    print(res)
    print(type(res))
    print(a.shape)
    print(a.graph)
    print(a.op)
d=tensorflow.zeros([2,3],dtype=tensorflow.float32)
print(d)
e=tensorflow.ones_like(d,dtype=tensorflow.float32)
with tensorflow.Session() as sess:
    print(sess.run(d))
    print(sess.run(e))
a=[[1,2,3],[4,5,6]]
b=[[7,8,9],[10,11,12]]
e=tensorflow.concat([a,b],axis=0)
f=tensorflow.concat([a,b],axis=1)
with tensorflow.Session() as sess:
    print(sess.run(e))
    print(sess.run(f))
   # tensorflow.summary.FileWriter("./testtensorflow/tensorboard",graph=sess.graph)
    #写成上面这样的时候提示FileWriter已经版本进行过替换了，在当前版本要写成下面这种形式
    #tf.compat.v1.get_default_graph
    #ensorflow.compat.v1.summary.FileWriter("./testtensorflow/tensorboard",graph=tensorflow.get_default_graph)
    #我们获取并显示默认的图
    tensorflow.compat.v1.summary.FileWriter("./testtensorflow/tensorboard", graph=tensorflow.compat.v1.get_default_graph())
