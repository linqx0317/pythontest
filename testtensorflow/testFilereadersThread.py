import tensorflow as tf
import os
import sys
filename=os.listdir("./testtensorflow/testdata")
#print(filename)
filepathlist=[]#这里用list是因为tuple不支持增删
#print(os.getcwd()+"："+str(sys.path[0]))
#上面这两个都是获取路径的，获取出来都是正确的
for ele in filename:
    filepathlist.append(os.getcwd()+"\\testtensorflow\\testdata\\"+str(ele))
print(str(filepathlist))
#这个被替换了
"""Queue-based input pipelines have been replaced by `tf.data`. Use `tf.data.Dataset.from_tensor_slices(string_tensor).shuffle(tf.
shape(input_tensor, out_type=tf.int64)[0]).repeat(num_epochs)`. If `shuffle=False`, omit the `.shuffle(...)"""
stringinputfilepath=tf.compat.v1.train.string_input_producer(filepathlist,shuffle=True)
#stringinputfilepath=tf.data.Dataset.from_tensor_slices(filepathlist).shuffle(tf.shape((1,2), out_type=tf.int64)[0])
#stringinputfilepath=tf.data.Dataset.from_tensor_slices(filepathlist).shuffle(tf.shape((1,2), out_type=tf.int64)[0])
#这个上面和下面都是是新一代的读取文件的写法，但是不用也没有与什么问题，暂时
#但是这个用法和学习的csv等文件读取的方法相差比较大，目前还有进行研究
#textLineReader = tf.TextLineReader()
filenames = tf.broadcast_to
textLineReader=tf.data.TextLineDataset(stringinputfilepath)
key,value=textLineReader.read(stringinputfilepath)
#ReaderReadV2(key=<tf.Tensor 'ReaderReadV2:0' shape=() dtype=string>, value=<tf.Tensor 'ReaderReadV2:1' shape=() dtype=string>)
# print(value)#这样就可以知道里面会有两个值
records=[[0],[0],[""]]
res1,res2,res3=tf.io.decode_csv(value,record_defaults=records)
# #最后这个record_defalut指定的是我们读出来的数据是什么类型的，默认值又是什么
# # textLineReader=tf.data.TextLineDataset(stringinputfilepath)
with tf.compat.v1.Session() as sess:
    #print(sess.run([res1,res2,res3]))
    print(res1.eval())
    print(sess.run(value))
    print(res2.eval())

    #tf.decode_csv(records,record_defaults=None,field_delim=None,name=None)



