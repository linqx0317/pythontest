import  pandas,numpy,string

# a =pandas.Series(numpy.arange(0,5),index=list(string.ascii_letters[:5]))
# print(a)
# b=a.astype(float)
# print(a)
# print(b)
datalist=[{"name":"linqx","age":21,"tel":"123","email":"911"},{"name":"linqx01","age":22,"tel":"124","email":"912"},{"name":"linqx03","age":23, "tel":"125"}]
c=pandas.DataFrame(datalist)
d=c.reindex(["第一","第二","第三"])
print(c)
print(c.values)
#print(d)
print(c.dtypes)
print(c.describe())
print(c[:2])
print(c[(c.loc[:,"age"]>21)&(c.loc[:,"email"]!=numpy.NaN)])
print(c[(c.loc[:,"age"]>21)&(pandas.notnull(c.loc[:,"email"]))])
print(c.loc[:,"email"].dtypes)
#print[c[:,:1]]

datalist=[{"name":"linqx","age":21,"tel":"123","email":"911"},{"name":"linqx01","age":22,"tel":"124","email":"912"},{"name":"linqx03","age":23, "tel":"125"}]
e=pandas.DataFrame(datalist)
d=pandas.DataFrame(numpy.arange(0,8).reshape(2,4),dtype="int")
print(d)
f=e.join(d)
print(f)
g=d.join(e)
print(g)
e.index=['a','b',"c"]
print(e)
e.set_index(["name","age"],drop=False,inplace=True)
print(e)
print(e.index)
print(e.loc["linqx"].loc[21])
#这里从外层开始获取内容，后面这个21是数字类型的千万不能写成字符类型，会直接报错
print(e.swaplevel().loc[21])#对于索引做反转，从内层开始获取
t=pandas.DataFrame(numpy.random.uniform(10,50,(100,1)),index=pandas.date_range(start="20170901",periods=100))
print(t)
print(t.resample("M").count())
print(t.resample("MS"))
t=[1,2,3,4,5,6,7]
print(t)
timedf=pandas.DataFrame(numpy.ones(shape=(4,5),dtype="int"),columns=["year","month","day","hour","min"])
print(timedf)
print(timedf["year"])
pa=pandas.PeriodIndex(year=timedf["year"],month=timedf["month"],day=timedf["day"],freq="3D")
print(pa)
timedf["periodindex"]=pa
print(timedf)


