#coding=utf-8
#绘制用户年龄分布
from matplotlib import   pyplot,font_manager
myfont=font_manager.FontProperties(fname="./static/STSONG.TTF")
a=[13,14,25,57,53,23,36,23,12,43,12,45,6,45,34,23,12,63,43,65,90]
width=[0,20,30,40,50,60,100]
pyplot.hist(a,width)#给定数据集，然后设置组距
pyplot.xticks(width,width)#设置x轴显示内容，以及实际的x轴的距离
pyplot.grid()#显示网格
pyplot.savefig("./static/histspic.svg")
pyplot.show()